import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/interpolation',
      name: 'interpolation',
      component: () => import('../views/Interpolation.vue')
    },
    {
      path: '/binding',
      name: 'binding',
      component: () => import('../views/Binding.vue')
    },
    {
      path: '/editstyle',
      name: 'editstyle',
      component: () => import('../views/editstyle.vue')
    },
    {
      path: '/event',
      name: 'event',
      component: () => import('../views/event_method.vue')
    },
    {
      path: '/dataArray',
      name: 'dataArray',
      component: () => import('../views/Data_Array.vue')
    },
  ]
})

export default router
